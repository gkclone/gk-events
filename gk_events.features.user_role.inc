<?php
/**
 * @file
 * gk_events.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function gk_events_user_default_roles() {
  $roles = array();

  // Exported role: event creator.
  $roles['event creator'] = array(
    'name' => 'event creator',
    'weight' => 2,
  );

  // Exported role: event editor.
  $roles['event editor'] = array(
    'name' => 'event editor',
    'weight' => 3,
  );

  return $roles;
}
