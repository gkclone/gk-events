<?php
/**
 * @file
 * gk_events_domain.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gk_events_domain_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
