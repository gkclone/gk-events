<?php

$plugin = array(
  'title' => 'Events: List',
  'category' => 'GK Events',
  'single' => TRUE,
);

function gk_events_gk_events_list_content_type_render($subtype, $conf, $args, $context) {
  // Build an array of options to pass to gk_events_get_items().
  $options = array(
    'filters' => array(
      'promote' => $conf['promote'],
      'sticky' => $conf['sticky'],
    ),
    'paged' => $conf['paged'],
    'per_page' => $conf['per_page'],
    'exclude_current_node' => $conf['exclude_current_node'],
  );

  $date_fields = array(
    'upcoming' => 'date_from',
    'past' => 'date_to',
  );
  $options['filters'][$date_fields[$conf['type']]] = time();

  if (!empty($conf['categories'])) {
    $categories = $conf['categories'];

    if (isset($categories['all'])) {
      // Ensure that no category filtering happens.
      $categories = array();
    }
    elseif (isset($categories['current'])) {
      // Ensure that we filter by the current term, at least.
      if (empty($categories)) {
        $categories = array(-1 => -1);
      }

      if ($term = menu_get_object('taxonomy_term', 2)) {
        $categories[$term->tid] = $term->tid;
      }
    }

    $options['filters']['categories'] = $categories;
  }

  if ($conf['limit']) {
    $options['limit'] = $conf['limit'];
  }

  if ($events = gk_events_get_items($options)) {
    $content = array();

    foreach ($events as $event) {
      ObjectMetaDataStorage::set($event, $conf);
      $content[$event->nid] = node_view($event, $conf['view_mode']);
    }

    if ($options['paged']) {
      $content['pager'] = array(
        '#theme' => 'pager',
      );
    }

    return (object) array(
      'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
      'content' => $content,
    );
  }
}

function gk_events_gk_events_list_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  // Compile a list of view modes for the view mode select form
  $entity_info = entity_get_info('node');
  $view_modes = array('default' => t('Default'));

  foreach ($entity_info['view modes'] as $key => $item) {
    if ($item['custom settings']) {
      $view_modes[$key] = $item['label'];
    }
  }

  $form['view_mode'] = array(
    '#type' => 'select',
    '#title' => t('View Mode'),
    '#options' => $view_modes,
    '#default_value' => isset($conf['view_mode']) ? $conf['view_mode'] : 'teaser',
  );

  $form['type'] = array(
    '#type' => 'select',
    '#title' => 'Type',
    '#options' => array(
      'upcoming' => 'Upcoming',
      'past' => 'Past',
    ),
    '#required' => TRUE,
    '#default_value' => isset($conf['type']) ? $conf['type'] : NULL,
  );

  $category_options = array(
    'all' => 'All',
    'current' => 'Current term',
  );

  if ($vocabulary = taxonomy_vocabulary_machine_name_load('event_categories')) {
    if ($categories = taxonomy_get_tree($vocabulary->vid)) {
      foreach ($categories as $term) {
        $category_options[$term->tid] = $term->name;
      }
    }
  }

  $form['categories'] = array(
    '#type' => 'select',
    '#title' => 'Categories',
    '#options' => $category_options,
    '#multiple' => TRUE,
    '#default_value' => isset($conf['categories']) ? $conf['categories'] : NULL,
  );

  $form['promote'] = array(
    '#type' => 'select',
    '#title' => 'Promoted',
    '#options' => array(
      1 => 'Yes',
      0 => 'No',
    ),
    '#empty_option' => 'Both',
    '#empty_value' => 'both',
    '#default_value' => isset($conf['promote']) ? $conf['promote'] : NULL,
  );

  $form['sticky'] = array(
    '#type' => 'select',
    '#title' => 'Stickied',
    '#options' => array(
      1 => 'Yes',
      0 => 'No',
    ),
    '#empty_option' => 'Both',
    '#empty_value' => 'both',
    '#default_value' => isset($conf['sticky']) ? $conf['sticky'] : NULL,
  );

  $form['paged'] = array(
    '#type' => 'checkbox',
    '#title' => 'Paged',
    '#default_value' => isset($conf['paged']) ? $conf['paged'] : 0,
  );

  $form['per_page'] = array(
    '#type' => 'textfield',
    '#title' => 'Per page',
    '#default_value' => isset($conf['per_page']) ? $conf['per_page'] : 5,
    '#element_validate' => array('element_validate_number'),
    '#states' => array(
      'visible' => array(
        ':input[name="paged"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['limit'] = array(
    '#type' => 'textfield',
    '#title' => 'Limit',
    '#default_value' => isset($conf['limit']) ? $conf['limit'] : 0,
    '#element_validate' => array('element_validate_number'),
  );

  $form['exclude_current_node'] = array(
    '#type' => 'checkbox',
    '#title' => 'Exclude current node',
    '#default_value' => isset($conf['exclude_current_node']) ? $conf['exclude_current_node'] : 1,
  );

  $form['hide_dates'] = array(
    '#type' => 'checkbox',
    '#title' => 'Hide dates',
    '#default_value' => isset($conf['hide_dates']) ? $conf['hide_dates'] : 0,
  );

  return $form;
}

function gk_events_gk_events_list_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];

  if ($form_state['conf']['promote'] == 'both') {
    $form_state['conf']['promote'] = NULL;
  }

  if ($form_state['conf']['sticky'] == 'both') {
    $form_state['conf']['sticky'] = NULL;
  }
}
