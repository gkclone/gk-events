<?php
/**
 * @file
 * gk_events_domain.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function gk_events_domain_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'domain_node_event';
  $strongarm->value = array(
    0 => 'DOMAIN_ACTIVE',
  );
  $export['domain_node_event'] = $strongarm;

  return $export;
}
