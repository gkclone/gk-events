<?php
/**
 * @file
 * gk_events.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function gk_events_taxonomy_default_vocabularies() {
  return array(
    'event_categories' => array(
      'name' => 'Event categories',
      'machine_name' => 'event_categories',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
