<?php

$plugin = array(
  'title' => 'Events: Categories',
  'category' => 'GK Events',
  'single' => TRUE,
);

function gk_events_gk_events_categories_content_type_render($subtype, $conf, $args, $context) {
  // Build an array of options to pass to gk_events_get_items().
  $options = array(
    'filters' => array(
      'promote' => $conf['promote'],
      'sticky' => $conf['sticky'],
    ),
    'exclude_current_node' => FALSE,
    'node_load' => FALSE,
  );

  $date_fields = array(
    'upcoming' => 'date_from',
    'past' => 'date_to',
  );
  $options['filters'][$date_fields[$conf['type']]] = time();

  if ($events = gk_events_get_items($options)) {
    $query = db_select('taxonomy_index', 'ti')
      ->distinct()
      ->fields('ti', array('tid'))
      ->condition('tv.machine_name', 'event_categories')
      ->condition('ti.nid', array_keys($events), 'IN')
      ->addTag('node_access');

    $query->join('taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
    $query->join('taxonomy_vocabulary', 'tv', 'ttd.vid = tv.vid');

    if ($tids = $query->execute()->fetchCol()) {
      if ($terms = taxonomy_term_load_multiple($tids)) {
        $items = array();

        foreach ($terms as $term) {
          $items[] = array(
            'data' => l($term->name, 'taxonomy/term/' . $term->tid),
            'class' => array(
              drupal_html_class($term->name),
            ),
          );
        }

        if (!empty($items)) {
          return (object) array(
            'title' => $conf['override_title'] ? $conf['override_title_text'] : 'Categories',
            'content' => array(
              '#theme' => 'item_list',
              '#items' => $items,
            ),
          );
        }
      }
    }
  }
}

function gk_events_gk_events_categories_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['type'] = array(
    '#type' => 'select',
    '#title' => 'Type',
    '#options' => array(
      'upcoming' => 'Upcoming',
      'past' => 'Past',
    ),
    '#required' => TRUE,
    '#default_value' => isset($conf['type']) ? $conf['type'] : NULL,
  );

  $form['promote'] = array(
    '#type' => 'select',
    '#title' => 'Promoted',
    '#options' => array(
      1 => 'Yes',
      0 => 'No',
    ),
    '#empty_option' => 'Both',
    '#empty_value' => 'both',
    '#default_value' => isset($conf['promote']) ? $conf['promote'] : NULL,
  );

  $form['sticky'] = array(
    '#type' => 'select',
    '#title' => 'Stickied',
    '#options' => array(
      1 => 'Yes',
      0 => 'No',
    ),
    '#empty_option' => 'Both',
    '#empty_value' => 'both',
    '#default_value' => isset($conf['sticky']) ? $conf['sticky'] : NULL,
  );

  return $form;
}

function gk_events_gk_events_categories_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];

  if ($form_state['conf']['promote'] == 'both') {
    $form_state['conf']['promote'] = NULL;
  }

  if ($form_state['conf']['sticky'] == 'both') {
    $form_state['conf']['sticky'] = NULL;
  }
}
