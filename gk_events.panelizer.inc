<?php
/**
 * @file
 * gk_events.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function gk_events_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:event:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'event';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '2_columns_75_25';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'primary' => NULL,
      'secondary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '19166602-490b-4cd3-bbd9-6bd32f9c39b6';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-73ee5ede-4277-45e3-a57c-8717df958036';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '73ee5ede-4277-45e3-a57c-8717df958036';
    $display->content['new-73ee5ede-4277-45e3-a57c-8717df958036'] = $pane;
    $display->panels['primary'][0] = 'new-73ee5ede-4277-45e3-a57c-8717df958036';
    $pane = new stdClass();
    $pane->pid = 'new-b339bda8-6bf9-4e1e-bb46-772180f3c73c';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'b339bda8-6bf9-4e1e-bb46-772180f3c73c';
    $display->content['new-b339bda8-6bf9-4e1e-bb46-772180f3c73c'] = $pane;
    $display->panels['primary'][1] = 'new-b339bda8-6bf9-4e1e-bb46-772180f3c73c';
    $pane = new stdClass();
    $pane->pid = 'new-00fb43e4-befe-4c28-830b-6d18b6ed63fd';
    $pane->panel = 'secondary';
    $pane->type = 'gk_events_categories';
    $pane->subtype = 'gk_events_categories';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'type' => 'upcoming',
      'promote' => NULL,
      'sticky' => NULL,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '00fb43e4-befe-4c28-830b-6d18b6ed63fd';
    $display->content['new-00fb43e4-befe-4c28-830b-6d18b6ed63fd'] = $pane;
    $display->panels['secondary'][0] = 'new-00fb43e4-befe-4c28-830b-6d18b6ed63fd';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:event:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:gk_events_upcoming';
  $panelizer->title = 'Events: Upcoming';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '2_columns_75_25';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'primary' => NULL,
      'secondary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '3e37bbed-03a5-4393-80a8-515035c346e8';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-73ef8c2a-2349-4e9b-b67a-3ffa3f376c48';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '73ef8c2a-2349-4e9b-b67a-3ffa3f376c48';
    $display->content['new-73ef8c2a-2349-4e9b-b67a-3ffa3f376c48'] = $pane;
    $display->panels['primary'][0] = 'new-73ef8c2a-2349-4e9b-b67a-3ffa3f376c48';
    $pane = new stdClass();
    $pane->pid = 'new-59ae54ef-0e9d-438c-b422-9a2e5a9415af';
    $pane->panel = 'primary';
    $pane->type = 'gk_events_list';
    $pane->subtype = 'gk_events_list';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'view_mode' => 'teaser',
      'type' => 'upcoming',
      'categories' => array(
        'all' => 'all',
      ),
      'promote' => NULL,
      'sticky' => NULL,
      'paged' => 1,
      'per_page' => '3',
      'limit' => '0',
      'exclude_current_node' => 0,
      'hide_dates' => 0,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '59ae54ef-0e9d-438c-b422-9a2e5a9415af';
    $display->content['new-59ae54ef-0e9d-438c-b422-9a2e5a9415af'] = $pane;
    $display->panels['primary'][1] = 'new-59ae54ef-0e9d-438c-b422-9a2e5a9415af';
    $pane = new stdClass();
    $pane->pid = 'new-b7a9748d-b6b2-435e-9983-b50e37a554a1';
    $pane->panel = 'secondary';
    $pane->type = 'gk_events_categories';
    $pane->subtype = 'gk_events_categories';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'type' => 'upcoming',
      'promote' => NULL,
      'sticky' => NULL,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b7a9748d-b6b2-435e-9983-b50e37a554a1';
    $display->content['new-b7a9748d-b6b2-435e-9983-b50e37a554a1'] = $pane;
    $display->panels['secondary'][0] = 'new-b7a9748d-b6b2-435e-9983-b50e37a554a1';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:gk_events_upcoming'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'taxonomy_term:event_categories:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'taxonomy_term';
  $panelizer->panelizer_key = 'event_categories';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '2_columns_75_25';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'primary' => NULL,
      'secondary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '969fdc4d-f83d-439b-bf86-f47499d1649d';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b564b230-97b7-4ffc-bc7e-76fe797efc3d';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b564b230-97b7-4ffc-bc7e-76fe797efc3d';
    $display->content['new-b564b230-97b7-4ffc-bc7e-76fe797efc3d'] = $pane;
    $display->panels['primary'][0] = 'new-b564b230-97b7-4ffc-bc7e-76fe797efc3d';
    $pane = new stdClass();
    $pane->pid = 'new-56981c0c-736e-4a36-98c5-12cc59051821';
    $pane->panel = 'primary';
    $pane->type = 'gk_events_list';
    $pane->subtype = 'gk_events_list';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'view_mode' => 'teaser',
      'type' => 'upcoming',
      'categories' => array(
        'current' => 'current',
      ),
      'promote' => NULL,
      'sticky' => NULL,
      'paged' => 1,
      'per_page' => '5',
      'limit' => '0',
      'exclude_current_node' => 1,
      'hide_dates' => 0,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '56981c0c-736e-4a36-98c5-12cc59051821';
    $display->content['new-56981c0c-736e-4a36-98c5-12cc59051821'] = $pane;
    $display->panels['primary'][1] = 'new-56981c0c-736e-4a36-98c5-12cc59051821';
    $pane = new stdClass();
    $pane->pid = 'new-785d3748-1486-4075-b207-70bdd8aac0f9';
    $pane->panel = 'secondary';
    $pane->type = 'gk_events_categories';
    $pane->subtype = 'gk_events_categories';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'type' => 'upcoming',
      'promote' => NULL,
      'sticky' => NULL,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '785d3748-1486-4075-b207-70bdd8aac0f9';
    $display->content['new-785d3748-1486-4075-b207-70bdd8aac0f9'] = $pane;
    $display->panels['secondary'][0] = 'new-785d3748-1486-4075-b207-70bdd8aac0f9';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['taxonomy_term:event_categories:default'] = $panelizer;

  return $export;
}
